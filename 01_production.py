#!/usr/bin/env python3
# Production

# Compute trajectories for a batch of Vicsek model simulation
from datetime import datetime
import os
import logging
import glob

from atooms.core.utils import mkdir, setup_logging

from src.metadata import Metadata
from src.simulation import Simulations

# Production

# Instantiate classes
sim = Simulations()

# Paths
file_inp = os.path.join(os.path.dirname(__file__), 'input/')
file_out = os.path.join(os.path.dirname(__file__), 'data/')
# Make sure directories exist
if not os.path.exists(file_inp):
    mkdir(file_inp)
if not os.path.exists(file_out):
    mkdir(file_out)

# Setup logging
log = setup_logging(name='wkfl', level=logging.DEBUG, filename=file_out+'workflow.log')

# Instantiate metadata class
meta = Metadata(file_inp, file_out, db_name='db.json', log_init=True)

# Fetch metadata and create metadata log file
metadata = meta.load_metadata(database=False)

# Run simulations
if not glob.glob(file_inp + '*' + meta.trajectory_extension):
    sim.vicsek_batch_parallel(metadata, file_inp=file_inp, config_number=100, load=False, log=False)

    # NOTE: this is a patch hiding a more significant issue
    #       active api doens't easily allow to change output directory
    # Move trajectories to `input`
    for item in os.listdir(file_out):
        if item.endswith(('.xyz', '.xyz.chk', '.xyz.chk.step', '.xyz.log')):
            os.replace(os.path.join(file_out, item), os.path.join(file_inp, item)) 

# Rename trajectories with MD5 hash (instead of RandomChoiceID)
meta.randomchoice_to_md5()

# Update metadata log
meta.write_metadata(inp=file_inp)

log.info('{} : Production complete\n'.format(datetime.now()))
