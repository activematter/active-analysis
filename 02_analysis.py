#!/usr/bin/env python
# Analysis

import logging
import os
from datetime import datetime

from atooms.core.utils import setup_logging

from src.metadata import Metadata
from src.parameters import Phi, Theta
from src.helpers import _split_dict

# Analysis


# Paths
file_inp = os.path.join(os.path.dirname(__file__), 'input/')
file_out = os.path.join(os.path.dirname(__file__), 'data/')

# Setup logging
log = setup_logging(name='wkfl', level=logging.DEBUG, filename=file_out+'workflow.log')
log.info('{} : Analysis started'.format(datetime.now()))

# Instantiate classes
meta = Metadata(file_inp, file_out, db_name='db.json', log_init=False)

# Load metadata file or database
metadata = meta.load_metadata(database=False)
list_metadata = _split_dict(metadata)

# Iterate over each UID, load trajectory

for simulation in list_metadata:
    log.info('{} : Analyzing "{}"'.format(datetime.now(), simulation['uid']))
    # Load trajectory with proper UID
    trajectory_filename = simulation['uid'] + '.xyz'
    trajectory_path = os.path.join(file_inp, trajectory_filename)

    # Instantiate Parameters classes and set output dirs
    phi = Phi(trajectory_path, simulation)
    theta = Theta(trajectory_path, simulation)
    phi.output_path = file_out
    theta.output_path = file_out

    # Compute parameter properties and write to file
    phi.do()
    theta.do()

log.info('{} : Analysis complete'.format(datetime.now()))

'''
	# Test routine for local order parameter
	trajectory = TrajectoryXYZ(trajectory_path, 'r')
	phi.number_subcells = 2
	for idx, system in enumerate(trajectory):
		step = trajectory.steps[idx]
		phi.compute(system, step, scope='local')
'''
