#!/usr/bin/env python
# Compare analysis data with reference run
import logging
import os
from collections import defaultdict
from datetime import datetime

from atooms.core.utils import setup_logging

from src.metadata import Metadata
from src.parameters import Phi, Theta
from src.helpers import _split_dict

# NOTE: workflow is
#       1. fetch dataset metadata log
#       2. fetch analysis data from reference and from current analysis
#       2b. compare metadata logs
#       3. load data (using Parameter classes)
#       4. compare data line by line


# Paths
current_directory = os.getcwd()
file_inp = os.path.join(current_directory, 'input/')
file_ref = os.path.join(current_directory, 'reference/')
file_out = os.path.join(current_directory, 'data/')

# Set up logging
log = setup_logging(name='wkfl', level=logging.DEBUG, filename=file_out+'workflow.log')
log.info('{} : Comparison started'.format(datetime.now()))
log.info("Reference directory: {}".format(file_ref))

# Check if data folder and reference folder is empty or without analysis data
if (os.listdir(file_out) == 0 or ('.als.' in os.listdir(file_out))):
    raise Exception("Data is missing - either trajectories or analysis data.")
if (os.listdir(file_ref) == 0 or ('.als.' in os.listdir(file_ref))):
    raise Exception("Data is missing - either trajectories or analysis data.")

# Fetch metadata
meta_ref = Metadata(file_inp, file_ref+'input/', log_init=False)
meta = Metadata(file_inp, file_out, log_init=False)
metadata_ref = meta_ref.load_metadata(database=False)
metadata = meta.load_metadata(database=False)
list_metadata = _split_dict(metadata)

# Compare reference and analysis hashes
assert set(metadata['uid']) == set(metadata_ref['uid']), "dataset from analysis doesn't match reference. "

# Initialize batch variables dict
batch_results = defaultdict(list)

# Iterate over each UID, load trajectory
for simulation in list_metadata:
    # Load trajectory with proper UID
    trajectory_filename = simulation['uid'] + '.xyz'
    trajectory_path = os.path.join(file_inp, trajectory_filename)

    # Get reference analysis data
    reference_phi = os.path.join(file_ref+'data/', simulation['uid']+'.als.phi')
    reference_theta = os.path.join(file_ref+'data/', simulation['uid']+'.als.theta')

    # Instantiate desired parameter classes
    phi = Phi(trajectory_path, simulation)
    theta = Theta(trajectory_path, simulation)
    phi.output_path = file_out
    theta.output_path = file_out

    # Compare data for each trajectory
    phi.compare(reference_phi)
