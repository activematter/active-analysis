# Dictionary handling


def _split_dict(merged):
    """Split dictionary of lists into list of dictionaries with same keys."""
    dicts = [dict(zip(merged, i)) for i in zip(*merged.values())]
    return dicts


def _merge_dict(dicts):
    """Merge multiple dictionaries with same keys."""
    # Imports
    from collections import defaultdict

    merged = defaultdict(list)
    for d in dicts:
        for key, value in d.items():
            merged[key].append(value)
    return merged

# Hashes


def _md5_hash(file_inp):
    """
    Generate UID using MD5 algorithm.
    Hash can be generated using input file, e.g. trajectory
    """
    import hashlib
    with open(file_inp, "rb") as fh:
        data = fh.read()
    return hashlib.md5(data).hexdigest()
