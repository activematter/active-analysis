from datetime import datetime
import os
import logging

import atooms.active.api as api

# Simulation facilities


class Simulations:
    def __init__(self):
        # TODO: populate init (e.g. with in/out dirs)
        self.logger = logging.getLogger('wkfl')

    def _done_callback(self, future):
        self.logger.debug("{} : {} completed".format(datetime.now(), future.name))

    def vicsek_instance(self, noise, npart, eta, rho, v0, nsteps, seed, uid, file_inp='input/', data_log='data/metadata.log', log=True, load=False, config_number=1, extension='.xyz'):
        """API wrapper to dumb down interface for parallelization."""

        # Check if file_inp is a directory; if so, use UID to get input file name
        if load and os.path.isdir(file_inp) and not os.path.isfile(file_inp):
            file_inp = file_inp + uid + extension
            assert os.path.exists(file_inp), "Input file {} does not exist".format(file_inp)

        api.vm(file_inp, load=load, save=False, noise=noise,
               npart=npart, eta=eta, rho=rho, v0=v0, uid=uid, nsteps=nsteps, seed=seed,
               log=log, data_log=data_log, config_number=config_number)

    def vicsek_batch_serial(self, metadata, **kwargs):
        """Run (in parallel) multiple simulation using ready-made metadata."""
        # TODO: perhaps refactor with strategy, together with parallel?

        self.logger.debug("{} : {} tasks to be processed.".format(datetime.now(), len(metadata['noise'])))
        for i in range(len(metadata['noise'])):
            # Check if UID is in metadata dictionary; else, set to None
            uid = metadata['uid'][i]
            if 'uid' not in metadata.keys():
                uid = None
            self.vicsek_instance(metadata['noise'][i], metadata['npart'][i], metadata['eta'][i], metadata['rho']
                                 [i], metadata['v0'][i], metadata['nsteps'][i], metadata['seed'][i], uid, **kwargs)
            self.logger.debug("F-{} completed at {}".format(i, datetime.now()))
        self.logger.debug("\n{} tasks completed successfully".format(len(metadata['noise'])))

    def vicsek_batch_parallel(self, metadata, max_workers=5, **kwargs):
        """Run (in parallel) multiple simulation using ready-made metadata."""
        # TODO: perhaps refactor with strategy, together with series?
        # TODO: might be worth it using alternatives to concurrent (e.g. multiprocessing, mpi4py)
        from concurrent.futures import ProcessPoolExecutor
        # Process pool
        futures = []
        with ProcessPoolExecutor(max_workers=max_workers) as process_pool:
            if max_workers == None:
                max_workers = process_pool._max_workers - 1
            self.logger.debug("{} : {} tasks to be submitted to pool.".format(datetime.now(), len(metadata['noise'])))
            for i in range(len(metadata['noise'])):
                # Check if UID is in metadata dictionary; else, set to None
                if 'uid' not in metadata.keys():
                    uid = None
                else:
                    uid = metadata['uid'][i]
                f = process_pool.submit(self.vicsek_instance, metadata['noise'][i], metadata['npart'][i], metadata['eta']
                                        [i], metadata['rho'][i], metadata['v0'][i], metadata['nsteps'][i], metadata['seed'][i], uid, **kwargs)
                f.name = "F-{}".format(i)
                f.add_done_callback(self._done_callback)
                futures.append(f)
            self.logger.debug("{} : All Tasks submitted to pool.".format(datetime.now()))
            completed = [future.result() for future in futures if not future.cancelled()]
            self.logger.debug("{} : {} Futures Completed successfully".format(datetime.now(), len(completed)))
