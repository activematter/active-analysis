import os
import time
import logging

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as tic
import imageio
from atooms.trajectory import TrajectoryXYZ

_logger = logging.getLogger('wkfl')


def plot(x, y, out, title=None, filename='plot', extension='.png', kind='line', show=False, close=True, grid=False, skip=None, xlab=None, ylab=None, xlim=None, ylim=None, xerr=None, yerr=None, number_minor_x=None, number_minor_y=None, skiperr=1, label=None, legend=False, size=[5, 5], bbox_inches='tight'):
    """
    Generic plot routine.
    Arguments include plot title, plot line type, grid presence, data point skip, axes labels and limits, error bars on data, legend, plot size.
    """

    fig, ax = plt.subplots(figsize=size, tight_layout={'pad': 0})

    # Plot type
    # TODO: refactor
    types = ['scatter', 'line', 'scatterline', 'semilogx', 'semilogy', 'loglog']
    if grid:
        plt.grid(linestyle='dashed')
    if kind == types[0]:
        plt.scatter(x, y, label=label)
    if kind == types[1]:
        plt.plot(x, y, markevery=skip, label=label)
    if kind == types[2]:
        plt.plot(x, y, '-o', markevery=skip, label=label)
    if kind == types[3]:
        plt.semilogx(x, y, label=label)
    if kind == types[4]:
        plt.semilogy(x, y, label=label)
    if kind == types[5]:
        plt.loglog(x, y, label=label)

    # Plot titles
    if title != None:
        plt.title(title)

    # Axis label
    plt.xlabel(xlab)
    plt.ylabel(ylab)

    # Axis limits
    plt.xlim(xlim)
    plt.ylim(ylim)

    # Ticks
    ax.minorticks_on()
    ax.tick_params(which='both', direction='in', top=True, right=True)
    ax.tick_params(which='major', size=8)
    ax.tick_params(which='minor', size=5)
    if number_minor_x:
        ax.xaxis.set_minor_locator(tic.AutoMinorLocator(number_minor_x))
    if number_minor_y:
        ax.yaxis.set_minor_locator(tic.AutoMinorLocator(number_minor_y))

    # Errorbars
    if (np.any(xerr) != None or np.any(yerr) != None):
        plt.errorbar(x, y, fmt='x', yerr=yerr, xerr=xerr,
                     errorevery=skiperr, markevery=skiperr, capsize=2)

    # Legend
    if legend:
        plt.legend(frameon=False)
    if show:
        plt.show()

    plt.savefig(os.path.join(out, filename), bbox_inches=bbox_inches)

    if close:
        plt.close(fig)

    return fig, ax


class Plot:
    def __init__(self, out):
        # TODO: check if directory is directory

        # Directories
        self.trajectory_inp = None
        """Input directory containing trajectories for arrow plots."""
        self.out = out
        """Main output directory."""
        # Create `movie_frames/`
        self.frame_out = os.path.join(self.out, 'movie_frames/')
        """Output directory for frames used in trajectory movie."""
        if not os.path.exists(self.frame_out):
            os.makedirs(self.frame_out)

        # Extensions
        self.plot_extension = '.eps'
        """General image extension."""
        self.frame_extension = '.png'
        """Movie frame extension."""
        self.movie_extension = '.gif'
        """File extension for trajectory movie."""

    def plot(self, x, y, **kwargs):
        """Wrapper for generic plot function. It allows use of class variables."""
        plot(x, y, self.out, extension=self.plot_extension, **kwargs)

    def arrowplot(self, position, orientation, L, step=0, color=False, noaxes=True):
        """Draw arrow plot for trajectory movie."""

        # Image size is fixed due to arrow scaling
        plt.rcParams["figure.figsize"] = (10, 10)
        x, y = zip(position)
        u = np.cos(orientation)
        v = np.sin(orientation)
        plt.xlim(-L/2., L/2.)
        plt.ylim(-L/2., L/2.)
        if color:
            plt.scatter(x, y, c=orientation, marker='dash')
        else:
            plt.quiver(x, y, u, v, orientation, cmap='twilight', pivot='mid', width=0.0035,
                       scale_units='width', scale=40., headaxislength=6, headlength=6)
        if noaxes:
            plt.axis('off')
        plt.savefig(os.path.join(self.frame_out, 'step'+str(step).zfill(7)+self.frame_extension), bbox_inches='tight')
        plt.close('all')

    def movie(self, filename='movie', fps=None, duration=200):
        """Create gif from images in folder."""
        if fps != None:
            import warnings
            warnings.warn('`fps` keyword is not available anymore in ImageIO. Use `duration`', DeprecationWarning)
            duration = 1000 * 1./fps

        images = []
        with imageio.get_writer(os.path.join(self.out, filename+self.movie_extension), mode='I', duration=duration) as writer:
            for item in sorted(os.listdir(self.frame_out)):
                image = imageio.imread(os.path.join(self.frame_out, item))
                writer.append_data(image)

        _logger.info("GIF saved in {}" .format(os.path.join(self.out, filename+self.movie_extension)))

    def arrowplot_movie(self, skip=10, movie_name='vicsek', duration=200, fps=None, rmfiles=True, color=False):
        """Generate movie and partial plot from trajectory file - acts like a driver."""

        if self.trajectory_inp != None:
            with TrajectoryXYZ(self.trajectory_inp, 'r') as th:
                steps = th.steps[::skip]
                for i in range(len(steps)):
                    system = th[i]
                    L = system.cell.side[0]
                    # TODO: explore view
                    position = system.dump('position', order='F')
                    orientation = system.dump('orientation')
                    self.arrowplot(position, orientation, L, step=steps[i], color=color)
        # Cautionary wait to ensure files are loaded correctly - might remove
        time.sleep(2)
        self.movie(filename=movie_name, fps=fps, duration=duration)
        time.sleep(2)

        # Remove single frames from folder
        if rmfiles:
            for item in os.listdir(self.frame_out):
                if item.endswith(self.frame_extension):
                    os.remove(os.path.join(self.frame_out, item))
