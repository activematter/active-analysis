import os
import logging
import re
import csv
from datetime import datetime

import numpy as np

from atooms.core.utils import tipify, mkdir, cp, rmf, canonicalize
from atooms.trajectory import Trajectory, TrajectoryXYZ
from atooms.database import Database, pprint

from src.helpers import _merge_dict, _md5_hash

# Set up logging
_logger = logging.getLogger('wkfl')


def _metadata_from_active(entry):
    """Fork of metadata_from_atooms in atooms.database.hooks, with added metadata """
    from atooms.system.particle import distinct_species, composition

    path = entry['absolute_path']
    if not os.path.exists(path):
        # Exit gracefully. This is ok if files are removed and are
        # then cleared with update()
        return {}

    last_modified = os.path.getmtime(path)
    if '__cache_metadata_from_active' in entry:
        if entry['__cache_metadata_from_active'] >= last_modified:
            return {}

    # Store metadata
    th = Trajectory(path)
    system = th[0]
    db = {}
    db['__cache_metadata_from_active'] = os.path.getmtime(th.filename)
    s = re.search(r'([a-zA-Z0-9]*)Trajectory([a-zA-Z0-9]*)', str(th.__class__))
    fmt = (s.group(1) + s.group(2)).lower()
    db['format'] = fmt
    db['frames'] = len(th)
    db['megabytes'] = int((os.path.getsize(th.filename) / 1e6))
    db['particles'] = len(system.particle)
    db['species'] = ', '.join(distinct_species(system.particle))
    db['composition'] = dict(composition(system.particle))
    db['density'] = round(system.density, 10)
    if system.cell is not None:
        db['cell side'] = str(list(system.cell.side))[1: -1]
        db['cell volume'] = system.cell.volume
    if len(th) > 1:
        db['steps'] = int(th.steps[-1])
        db['duration'] = int(th.times[-1])
        db['timestep'] = float(th.timestep)
        db['block size'] = int(th.block_size)
    db['eta'] = th.metadata['eta']
    db['noise'] = th.metadata['noise']
    db['v0'] = th.metadata['v0']
    db['seed'] = th.metadata['seed']
    th.close()
    return db


def _missing_metadata(rho=None, npart=None, L=None):
    """Generate missing metadata from what's available."""
    if rho is None and npart is not None and L is not None:
        rho = npart/L**2
    elif rho is not None and npart is None and L is not None:
        npart = rho*(L**2)
    elif rho is not None and npart is not None and L is None:
        L = np.sqrt(npart/rho)
    return rho, npart, L


class Metadata:
    """Handle input data for simulations using atooms.active; generate proper metadata and store it in metadata logs or databases"""

    def __init__(self, file_inp, file_out, metadata_name='metadata.log', db_name='database/db.json', trajectory_extension='.xyz', log_init=False):
        # NOTE: database usage is WIP

        self.file_inp = file_inp
        """Input directory containing trajectories"""
        self.file_out = file_out
        """Output directory for metadata log/database"""
        self.metadata_out = os.path.join(file_inp, metadata_name)
        """Path for metadata output. Defaults to input directory."""
        self.db_out = os.path.join(file_inp, db_name)
        """Path for database output. Defaults to input directory."""

        self.trajectory_extension = trajectory_extension
        self.keys = ['npart', 'rho', 'eta', 'v0',
                     'nsteps', 'noise', 'seed', 'uid']
        self.thesaurus = {'particles': 'npart',
                          'density': 'rho',
                          'steps': 'nsteps',
                          'md5_hash': 'uid'
                          }
        # Initialize logger
        self.logger = _logger
        if log_init:
            _logger.info("active data analysis\n")
            _logger.info("{} : workflow started".format(datetime.now()))
            _logger.info("Input directory: {}".format(self.file_inp))
            _logger.info("Output directory: {}".format(self.file_out))
            _logger.info("Metadata file: {}".format(self.metadata_out))
            _logger.info("Database file: {}\n".format(self.db_out))

    def load_metadata(self, database=False):
        """Load metadata file or generate one from trajectory to gather metadata"""

        # TODO: check if UID in .log and .xyz coincide
        import glob
        assert os.path.isdir(self.file_inp), "Input path is not a directory"

        # Check if input directory contains trajectories and if there is any metadata log

        if glob.glob(self.file_inp + '*' + self.trajectory_extension) and (not os.path.exists(self.metadata_out) or not os.path.exists(self.db_out)):
            if database:
                _logger.debug("{} : Trajectories found, database created.".format(datetime.now()))
                self.write_database()
                assert os.path.exists(
                    self.db_out), "Error in generating database"
            else:
                _logger.debug("{} : Trajectories found, metadata log created.".format(datetime.now()))
                self.write_metadata()
                assert os.path.exists(
                    self.metadata_out), "Error in generating metadata .log"
        elif not glob.glob(self.file_inp + '*' + self.trajectory_extension) and not (os.path.exists(self.metadata_out) or os.path.exists(self.db_out)):
            _logger.critical("{} : No .xyz files found in {}.".format(datetime.now(), self.file_inp))
            raise Exception(
                "No .xyz files found in {}." .format(self.file_inp))
        if database:
            metadata = self.read_database()
            _logger.debug("{} : Reading from database.".format(datetime.now()))
        else:
            metadata = self.read_metadata()
            _logger.debug("{} : Reading from metadata log.".format(datetime.now()))
        return metadata

    def read_metadata(self):
        """Read metadata from metadata file and pass to vicsek_batch."""

        # Open metadata file
        with open(self.metadata_out, 'r') as f:
            data_raw = csv.DictReader(f, delimiter=',')
            metadata_batch = _merge_dict(list(data_raw))
        # Tipify strings in merged dictionary
        for k in metadata_batch.keys():
            metadata_batch[k] = [tipify(item) for item in metadata_batch[k]]
        return metadata_batch

    def write_metadata(self, metadata=None, inp=None):
        """
        Write metadata to file from ongoing simulation OR ready-made trajectory.
        Input metadata must be properly parsed (*i.e.* a dictionary).
        """
        # Check if metadata file already exists
        uids_in_file = []

        if os.path.exists(self.metadata_out) and os.stat(self.metadata_out).st_size != 0:
            # Fetch UID list
            uids_in_file = np.genfromtxt(
                self.metadata_out, skip_header=1, delimiter=',', usecols=(-1), dtype=str).T
        # Initialize metadata file
        mkdir(os.path.dirname(self.metadata_out))
        f = open(self.metadata_out, 'w')
        write = csv.writer(f)
        # Add header if file is empty
        if os.stat(self.metadata_out).st_size == 0:
            write.writerow(['npart', 'rho', 'eta', 'v0',
                           'nsteps', 'noise', 'seed', 'uid'])
        # Initialize proper input directory
        if inp is None:
            inp = self.file_inp
        else:
            assert os.path.isdir(inp), f"specified input directory {inp} isn't a directory"

        # Check for existing trajectories in directory and extract metadata
        # Check whether directory exists, contains files, and if they have proper extension
        if metadata is None and os.path.exists(inp) and os.listdir(inp):
            for item in os.listdir(inp):
                if item.endswith(self.trajectory_extension):
                    # Strip extension from trajectory filename (assuming traj. filename == uid)
                    trajectory_filename = re.sub(
                        self.trajectory_extension, '', item)

                    # NOTE: this snippet is problematic, for the log file gets
                    # overwritten nonetheless!
                    # Check if uid is in existing uids in file
                    # if trajectory_filename in uids_in_file:
                    #    continue

                    step_filename = inp + item + '.chk.step'
                    assert os.path.exists(
                        step_filename), f"nsteps data file {step_filename} is missing"
                    # Get metadata from trajectory and nsteps from '.chk.step' file
                    with open(step_filename, 'r') as st:
                        nsteps = int(st.read())
                    with TrajectoryXYZ(inp + item, mode='r') as th:
                        # We assume uid is contained in trajectory_filename
                        rho = round(
                            th.metadata['npart']/(th.metadata['cell'][0]**2), 8)
                        meta = [th.metadata['npart'], rho, th.metadata['eta'], th.metadata['v0'],
                                nsteps, th.metadata['noise'], th.metadata['seed'], trajectory_filename]
                        write.writerow(meta)
        elif metadata is not None:
            assert isinstance(
                metadata, dict), "metadata isn't properly parsed in dict"
            meta = [metadata['npart'], metadata['rho'], metadata['eta'], metadata['v0'],
                    metadata['nsteps'], metadata['noise'], metadata['seed'], metadata['uid']]
            write.writerow(meta)
        f.close()

    def write_database(self, model='vicsek', verbose=False):
        """Create trajectory database from available trajectories - using atooms.database."""

        # Sanity check
        assert not os.path.exists(self.db_out), 'database already exists.'
        # Generate database
        mkdir(os.path.dirname(self.db_out))
        db = Database(self.db_out)
        # Fetch trajectories
        db.insert_glob(self.file_inp + '*' +
                       self.trajectory_extension, model=model)
        # Add hooks with active metadata
        db.add_hook(_metadata_from_active)
        db.update()
        if verbose:
            pprint(db.all(), columns=('particles', 'density', 'eta', 'v0',
               'steps', 'frames', 'noise', 'seed', 'md5_hash'), max_rows=10)

    def read_database(self, verbose=False):
        """Fetch metadata from database - using atooms.database."""
        # raise NotImplementedError('WIP')

        # Load database
        db = Database(self.db_out)
        assert len(db.all()) != 0, "database is empty"
        if verbose:
            pprint(db.all(), columns=('particles', 'density', 'eta', 'v0', 'steps', 'frames', 'noise', 'seed', 'md5_hash'), max_rows=10)
        # Merge metadata dictionaries into a single one
        metadata_raw = _merge_dict(db.all())
        # Canonicalize dictionary keys
        metadata_raw = dict(
            zip(canonicalize(metadata_raw.keys(), self.thesaurus), metadata_raw.values()))
        # Select relevant metadata based on keys
        metadata = {k: metadata_raw[k] for k in self.keys}
        return metadata

    def randomchoice_to_md5(self, remove=True):
        """
        Convert random_choice UID to MD5 hash in all files
        Note: MD5 hashes need a finished trajectory to be generated. Since UIDs in `api.py` are assigned before trajectory generation, MD5 hashes are not suitable as UIDs in `api.py`. Still, they can replace random UIDs after the fact - this method fills that gap.
        NOTE: ideally this belongs directly to `active` API
        """
        # Initialize thesaurus linking MD5 hashes to RCID
        uid_thesaurus = {}

        for item in os.listdir(self.file_inp):
            if item.endswith(self.trajectory_extension):
                file_inp = os.path.join(self.file_inp, item)
                # Get trajectory file and extract UID
                random_choice_uid = re.sub(self.trajectory_extension, '', item)
                # Generate hash based on binary read of file
                md5_hash_uid = _md5_hash(file_inp)
                uid_thesaurus[random_choice_uid] = md5_hash_uid
                # Check whether UID already is MD5 hash
                if random_choice_uid == md5_hash_uid:
                    continue
                else:
                    # Look for files with specified UID
                    for i in os.listdir(self.file_inp):
                        # Rename all files with UID with MD5 hash
                        if random_choice_uid in i:
                            i = os.path.join(self.file_inp, i)
                            i_renamed = re.sub(
                                random_choice_uid, md5_hash_uid, i)
                            cp(i, i_renamed)
                            if remove:
                                rmf(i)
        self.logger.debug("MD5 thesaurus: %s", uid_thesaurus)
        # Update database with new paths
        if os.path.exists(self.db_out):
            db = Database(self.db_out)
            db.insert_glob(self.file_out+'*'+self.trajectory_extension)

        # Update UIDs in metadata file
        if os.path.exists(self.metadata_out):
            import fileinput
            for line in fileinput.input(self.metadata_out, inplace=True):
                line = line.rstrip()
                if not line:
                    continue
                for f_key, f_value in uid_thesaurus.items():
                    if f_key in line:
                        line = line.replace(f_key, f_value)

    def batch_to_md5(self, md5_hashes, remove=True):
        """
        Generate MD5 hash from .txt file containing batch of MD5 hashes.
        Useful to assign a unique MD5 hash to batches of trajectories.
        """

        # Generate file with MD5 hashes
        batch_file_to_hash = os.path.join(self.file_out, 'batch_hashes.txt')
        with open(batch_file_to_hash, 'w') as f:
            np.savetxt(f, md5_hashes, fmt='%s', delimiter='\n')

        # Generate hash for batch file
        batch_hash = _md5_hash(batch_file_to_hash)

        # Remove batch file if requested
        if remove:
            os.remove(batch_file_to_hash)
        return batch_hash
