import logging
from collections import defaultdict

import numpy as np
from atooms.trajectory import TrajectoryXYZ

from src.analysis import Analysis

_logger = logging.getLogger('wkfl')

# Base correlation class


class CorrelationBase(Analysis):
    """Base class for Parameters-like classes"""

    def __init__(self, **kwargs):
        self.skip_acf = 1
        """Skip time steps in acf calculation."""
        self.sliced_acf = slice(None)
        """Slice object. Allows to consider slices of time series for acf computation.This allows, e.g., to remove initial transients."""
        self.sliced_acf_time = slice(None)
        """Same as sliced_acf, used to select transient slice of normalized acf when computing correlation time."""
        self.acf_thesaurus = {'acf function': 'autocorrelation',
                              'normalized acf function': 'autocorrelation_norm'
                              }
        """Reference from common names to specific correlation variables."""
        super().__init__(**kwargs)

    def acf(self):
        """Autocorrelation function. """
        # TODO: use atooms.postprocessing and remove this altogether

        # Hotwire variables
        # TODO: check definitions of tau and t
        if isinstance(self.sliced_acf, slice):
            tau = self.time[self.sliced_acf]
            t = self.tau[self.sliced_acf]
            x = self.time_series[self.sliced_acf]
            x_avg = self.average
        elif self.sliced_acf == None:
            tau = self.time
            t = self.tau
            x = self.time_series
            x_avg = self.average

        # Initialize dictionaries
        cf = defaultdict(float)
        """Correlation function"""
        cnt = defaultdict(int)
        """Pair counter"""

        # Compute autocorrelation function
        for i in t:
            for i0 in range(0, len(x)-i, self.skip_acf):
                dtau = tau[i0+i] - tau[i0]
                cf[dtau] += (x[i0+i] - x_avg)*(x[i0] - x_avg)
                cnt[dtau] += 1

        dtau = sorted(cf.keys())
        self.autocorrelation = [cf[ti]/cnt[ti] for ti in dtau]
        self.autocorrelation_norm = self.autocorrelation / \
            self.autocorrelation[0]
        # Pad if sliced
        '''
        if isinstance(self.sliced_acf, slice):
            # Compute how many elements were sliced
            number_sliced = abs(len(self.time) - len(dtau))
            # Pad list
            pad = [float('nan') for _ in range(number_sliced)]
            self.autocorrelation = list(np.append(self.autocorrelation, pad))
            self.autocorrelation_norm = list(np.append(self.autocorrelation_norm, pad))
        '''

    @property
    def autocorrelation_time(self):
        """Compute autocorrelation time from autocorrelation function, assuming it follows exponential decay. """
        idx = np.abs(
            self.autocorrelation_norm[self.sliced_acf_time] - 1./np.exp(1))
        return self.time[idx.argmin()]

    @property
    def autocorrelation_time_error(self):
        """Compute error on autocorrelation time according to Allen-Tildesley."""
        return np.abs(np.sqrt(2*self.autocorrelation_time/self.steps)*(1-self.autocorrelation_time))

    @property
    def autocorrelation_error(self):
        """Compute error on autocorrelation function according to Allen-Tildesley."""
        return np.abs(np.sqrt(2*self.autocorrelation_time/self.steps)*(1-np.asarray(self.autocorrelation_norm)))
