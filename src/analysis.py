from copy import copy
import re
import os
import logging
import warnings
from datetime import datetime
from collections import defaultdict

import numpy as np
from atooms.trajectory import TrajectoryXYZ

_logger = logging.getLogger('wkfl')

# Base analysis class


def _dump(title, columns=None, command=None, version=None,
          description=None, note=None, parents=None, inline=False,
          comment='# ', extra_fields=None):
    """
    Return a string of comments filled with metadata.
    -------------------------------------------------------------
    Fork of function from atooms.postprocessing utilities.
    Credits go to Prof. Daniele Coslovich
    """

    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    if columns is not None:
        columns_string = ''.join(columns)

    metadata = [('title', title),
                ('columns', columns_string),
                ('date', date),
                ('command', command),
                ('version', version),
                ('parents', parents),
                ('description', description),
                ('note', note)]

    if extra_fields is not None:
        metadata += extra_fields

    if inline:
        fmt = '{}: {};'
        txt = comment + ' '.join([fmt.format(key, value) for key,
                                  value in metadata if value is not None])
    else:
        txt = ''
        for key, value in metadata:
            if value is not None:
                txt += comment + '{}: {}\n'.format(key, value)
    return txt + '\n'


class Analysis(object):
    """
    Run a complete analysis of input trajectories with metadata and output to file.
    It emulates and takes many features from atooms.postprocessing, starting from its structure.
    """

    def __init__(self, trajectory, metadata, ndim=2, **kwargs):
        # Metadata
        self.keys = ['npart', 'rho', 'eta', 'v0',
                     'nsteps', 'noise', 'seed', 'uid']
        self.npart = metadata['npart']
        self.rho = metadata['rho']
        self.eta = metadata['eta']
        self.v0 = metadata['v0']
        self.steps = metadata['nsteps']
        self.noise = metadata['noise']
        self.uid = metadata['uid']
        self.L = np.sqrt(self.npart/self.rho)
        self.ndim = ndim
        # TODO: add ndim to metadata

        # Trajectory
        # Check if trajectory is path or Trajectory obj
        if isinstance(trajectory, str):
            self.trajectory = TrajectoryXYZ(trajectory, 'r')
        else:
            self.trajectory = trajectory

        self.columns = ['time', 'time series', 'normalized acf function']
        """Fields to write in output file."""
        self.analysis = {}
        """Parameters resulting from analysis."""
        self.comments = None
        """User specifiable comments"""
        self.output_path = None
        self.output_filename = None
        self.output = None

        # Thesauri
        # TODO: fix limitations of separate read/write thesauri - find alternative

        self.write_thesaurus = {'avg': 'average',
                                'var': 'variance',
                                't_corr': 'autocorrelation_time'
                                }
        """Reference from common names to specific variables not readable from file.."""

    def _init_rw(self):
        """Initialize read-write paths"""
        # Output analysis file path
        self.output_filename = self.uid + '.als.' + \
            re.sub("[\(\[].*?[\)\]]", "", self.short_name)
        if self.output_path is None:
            self.output_path = os.path.dirname(self.trajectory.filename)
        if self.output is None:
            self.output = os.path.join(self.output_path, self.output_filename)

    def analyze(self, acf=True):
        """Run a complete analysis of trajectory data for the parameter."""

        for idx, system in enumerate(self.trajectory):
            # Load current system and step, then compute phi
            step = self.trajectory.steps[idx]
            self.compute(system, step)
            # self.compute_local(system, step)

        self.analysis[self.short_name + ' time average'] = self.average
        self.analysis[self.short_name + ' variance'] = self.variance
        if acf:
            self.acf()
            self.analysis[self.short_name + ' autocorrelation time'] = self.autocorrelation_time

    def read(self, log=True):
        """
        Read the entire parameter analysis data from file for each trajectory.
        Currently, this doesn't work for matrix, 'local' parameter analysis data.
        """
        # Initialize read-write facilities
        self._init_rw()

        # Read columns
        with open(self.output, 'r') as inp:
            # Get columns from file comments and parse, assuming columns info is ALWAYS in first line
            firstline = inp.readline()
            raw_cols_from_file = re.search(
                'columns:(.*); ', firstline).group(1).split(',')
            self.columns = [i.strip() for i in raw_cols_from_file]

            # Read data from file
            rawcols = np.genfromtxt(inp, unpack=True, dtype=None)

            # Sanity check on number of columns read from file
            assert len(rawcols) == len(
                self.columns), "Number of columns doesn't match data in file"

            # Iterate through columns and load data
            for idx, field in enumerate(self.columns):
                # time and time series are in db, and that's all it takes to compute every other variable
                # Hardcoded db and acf construction
                if field in self.db_thesaurus.keys():
                    setattr(
                        self, '_'+self.db_thesaurus[field], list(rawcols[idx]))
                elif field in self.acf_thesaurus.keys():
                    setattr(
                        self, self.acf_thesaurus[field], list(rawcols[idx]))
                elif field in self.write_thesaurus.keys():
                    _logger.warn(f"Variable {field} is read-only, ignoring.")
                    continue
                else:
                    raise NameError(f"Variable {field} doesn't exist")

            # Load file data in db
            self.db = dict(zip(self._time, self._time_series))

        if log:
            _logger.info('{} : Fetched "{}" data from file {}'.format(datetime.now(), self.short_name, self.output))

    def write(self):
        """
        Write the entire parameter analysis data for each trajectory to file

        Default output path is simply defined by instance variable `output` as '{uid}.als.{short_name}'
        -------------------------------------------------------------
        Simplified fork of Correlation.write() method from atooms.postprocessing
        """

        self._init_rw()
        self.thesaurus = self.write_thesaurus | self.acf_thesaurus | self.db_thesaurus

        # Build array of arrays to dump
        # TODO: allow for more precision in values written to file (i.e. with specific format info for each data type, e.g. int->%d, float->%.8f etc...)
        x, fmt = [], []

        for field in self.columns:
            # Try bare column name, if it fails use thesaurus
            try:
                x.append(getattr(self, field))
            except:
                if field in self.thesaurus.keys():
                    x.append(getattr(self, self.thesaurus[field]))
        if not x:
            x = [self.time, self.time_series]
            _logger.warn(
                f'{field} is not a recognized field. Using default {x}')

        dump = np.transpose(np.asarray(x))

        # Comment line
        columns = ', '.join(self.columns)
        comments = _dump(title='%s [%s]' % (self.name, self.short_name),
                         columns=columns,
                         inline=True)
        if self.comments is not None:
            comments += self.comments

        # Results of analysis
        analysis = ""
        for x, f in self.analysis.items():
            if f is not None:
                analysis += '# %s: %s\n' % (x, f)

        # Put it all together
        # and make sure the path to the output file exists
        from atooms.core.utils import mkdir
        if self.output is not None:
            mkdir(os.path.dirname(self.output))
            with open(self.output, 'w') as fh:
                fh.write(comments)
                if len(analysis) > 0:
                    fh.write(analysis)
                np.savetxt(fh, dump, fmt="%g")
                fh.flush()

        _logger.info('{} : Saved "{}" data to file {}'.format(datetime.now(), self.short_name, self.output))

    def compare(self, to_compare):
        """
        Compare two analysis files.
        Note: since only "raw" analysis data is loaded from analysis files, that's the only data we need to compare, i.e. fields in `db_thesaurus`, `acf_thesaurus`
        """
        # Sanity check
        try:
            os.path.exists(to_compare)
        except:
            raise IOError("file to compare doesn't exist")
            _logger.critical('{} : File to compare does not exist.'.format(datetime.now()))

        _logger.info('Comparison file: {}'.format(to_compare))

        # Read file and store extracted data
        self.read(log=False)
        _logger.info('Analysis file: {}'.format(self.output))
        time = copy(self.time)
        time_series = copy(self.time_series)
        autocorrelation_norm = copy(self.autocorrelation_norm)

        # Set file to compare as new input and load it
        self.output = to_compare
        self.read(log=False)
        self.output = None
        # Reset old input
        self._init_rw()

        # Compare the two sets of data

        if not time == self.time:
            warnings.warn('time data does not match, uid: {}.'.format(self.uid), RuntimeWarning)
            _logger.warn('{} : time data does not match.'.format(datetime.now(), self.uid))

        if not time_series == self.time_series:
            warnings.warn('time series data does not match, uid: {}.'.format(self.uid), RuntimeWarning)
            _logger.warn('{} : time series data does not match.'.format(datetime.now()))

        if not autocorrelation_norm == autocorrelation_norm:
            warnings.warn('normalized acf data does not match, uid: {}.'.format(self.uid), RuntimeWarning)
            _logger.warn('{} : normalized acf data does not match.'.format(datetime.now()))

        _logger.info('{} : All data matches.'.format(datetime.now()))

    def do(self):
        """Analyze and write to file."""
        self.analyze()
        self.write()
