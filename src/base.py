import re
import os
import logging
from collections import defaultdict

import numpy as np
from atooms.trajectory import TrajectoryXYZ

from src.analysis import Analysis

_logger = logging.getLogger('wkfl')

# Extra parameters and utilities


def reduced_noise(eta, eta_critical):
    """Reduced noise amplitude, according to Chaté, Ginelli (2007)."""
    return 1. - eta/eta_critical

# Base parameter class


class PropertiesBase(Analysis):
    """Base class for parameter properties"""

    def __init__(self, **kwargs):
        self.name = None
        self.short_name = None
        self.field = None
        """Trajectory field used in parameter computation."""
        self.db = {}
        """Dictionary which stores (step, parameter) as (key, value) pairs."""
        self.db_thesaurus = {'time': 'time',
                             'time series': 'time_series'
                             }
        """References to proper names of variables in db."""

        super().__init__(**kwargs)

    def parameter(self, variable):
        """Analytical form of the desired parameter, to be implemented by Parameter classes."""
        pass

    def compute(self, system, step):
        """Compute the selected parameter."""

        variable = system.view(self.field)
        self.db[step] = self.parameter(variable)

    @property
    def time(self):
        """Extract frame number from callback dictionary."""
        return list(self.db.keys())

    @property
    def dt(self):
        """Compute time interval between frames."""
        return list(np.diff(self.time, prepend=-self.time[1]))

    @property
    def tau(self):
        """Compute timesteps."""
        return list(np.asarray(self.time)//np.asarray(self.dt))

    @property
    def time_series(self):
        """Extract parameter time series from callback dictionary."""
        return list(self.db.values())

    @property
    def average(self):
        """Compute parameter average."""
        return np.mean(self.time_series)

    @property
    def variance(self):
        """Compute parameter variance."""
        variance = (np.mean(np.asarray(self.time_series)**2)-self.average**2)
        return variance

    def histogram(self, histogram_bins=20):
        """Generate histogram from parameter time series. It also computes bin centers for easy plotting."""
        self.histogram, self.bin_edges = np.histogram(
            self.time_series, density=True, bins=histogram_bins)
        self.bin_centers = (self.bin_edges[:-1] + self.bin_edges[1:]) / 2


class LocalBase(Analysis):
    """Base class for local properties of parameter"""
    # TODO: perhaps refactor and merge to PropertiesBase using Strategy?

    # NOTE: a simpler alternative to a generalized PropertiesBase class
    # would be duplicating the needed methods, e.g. time_series, average, etc.
    # NOTE: the level of generalization needed for local properties is... none.
    # The PropertiesBase methods work flawlessly with a grid of local parameters!
    # NOTE: it's still not optimized for read operations.

    def __init__(self, **kwargs):
        # NOTE: simulation cell MUST be a hypercube
        self.number_subcells = 10
        """Number of subcells."""
        self.db_local = defaultdict(list)
        """Dictionary which stores (step, local parameter list) as (key, value) pairs."""
        super().__init__(**kwargs)

    def _sort_particles(self, positions):
        """Sort particles in subcell grid. Particles are sorted via indices, in `self.in_cells`."""
        # Transpose and shift positions
        # Sanity check to ensure C ordering
        if positions.flags['F_CONTIGUOUS']:
            positions = np.transpose(positions)
        positions_shifted = positions - self.cell.center + self.box[0]/2.

        # Get subcell coordinates for each particle
        particles_subcell_coordinates = np.floor(
            positions_shifted/self.side_subcell)

        # Convert subcell coordinates to indices
        # Subcells are (theoretically) numbered from the lower left corner to the upper right,
        # proceeding vertically.
        # TODO: check cell numbering; multiplying first coord leads to horizontal counting
        particles_subcell_indices = (np.abs(
            particles_subcell_coordinates[:, 0]) * self.number_subcells + np.abs(particles_subcell_coordinates[:, 1])).astype(int)

        self.subcell_particles = [list(np.where(particles_subcell_indices == i)[
            0]) for i in range(self.total_subcells)]

    def compute_local(self, system, step):
        """
        Compute local parameter according to subcell subdivision.
        Akin to PropertiesBase.compute(). 
        """
        # NOTE: we also initialize `self.box`, assuming simulation cell size is constant
        # throughout steps of a trajectory

        # Setup subcell numbers
        self.total_subcells = self.number_subcells**self.ndim

        # Create grid structure
        self.cell = system.cell
        self.box = self.cell.side
        self.side_subcell = self.box/self.number_subcells
        """Length of subcell side"""

        # Compute or update particle list in subcells
        positions = system.view('position')
        self._sort_particles(positions)

        for i in range(self.total_subcells):
            # TODO: find a more efficient way
            # Positionality isn't needed since we're not looking for neighbors
            variable = system.view(self.field)[self.subcell_particles[i]]
            self.db_local[step].append(self.parameter(variable))

    @property
    def time_series_local(self):
        """Extract parameter time series from callback dictionary."""
        return list(self.db_local.values())

    @property
    def average_local(self):
        """Compute parameter average."""
        return np.mean(self.time_series_local, axis=0)

    @property
    def variance_local(self):
        """Compute parameter variance."""
        variance = (np.mean(np.asarray(self.time_series_local)
                    ** 2)-self.average_local**2)
        return variance


'''
class MergedBase(Analysis):
	# TODO: some kind of refactoring is needed
	# NOTE: the issue with this setup is the inability to gather local and global data
	# at the same time. A way out could be checking if the shared variables have been
	# filled already; if so, the logged values get moved or saved alongside the new ones 
	# in appropriate data types - in both cases it must be user-accessible. 
	# The simpler alternative already is in place: separate local and global param facilities.
	# This reasoning is limited to self.db, since all other properties (tagged with the 
	# appropriate decorator) gather their data from that database.

	"""Base class for local and global parameter properties"""

	def __init__(self, **kwargs):
		"""NOTE: simulation cell MUST be a hypercube"""
		self.name = None
		self.short_name = None
		self.field = None
		"""Trajectory field used in parameter computation."""
		self.histogram_bins = 20
		"""Number of bins for histogram of parameter."""
		self.number_subcells = 10
		"""Number of subcells."""
		self.db = {}
		"""Dictionary which stores (step, parameter) as (key, value) pairs."""

		super().__init__(**kwargs)

	def parameter(self, variable):
		pass

	# Local parameter facilities
	def _sort_particles(self, positions):
		"""Sort particles in subcell grid. Particles are sorted via indices, in `self.in_cells`."""
		# Transpose and shift positions
		# Sanity check to ensure C ordering
		if positions.flags['F_CONTIGUOUS']:
			positions = np.transpose(positions)
		positions_shifted = positions - self.cell.center + self.box[0]/2.

		# Get subcell coordinates for each particle
		particles_subcell_coordinates = np.floor(
			positions_shifted/self.side_subcell)

		# Convert subcell coordinates to indices
		# Subcells are (theoretically) numbered from the lower left corner to the upper right, 
		# proceeding vertically.
		# TODO: check cell numbering; multiplying first coord leads to horizontal counting
		particles_subcell_indices = (np.abs(
			particles_subcell_coordinates[:, 0]) * self.number_subcells + np.abs(particles_subcell_coordinates[:, 1])).astype(int)

		self.subcell_particles = [list(np.where(particles_subcell_indices == i)[
									   0]) for i in range(self.total_subcells)]

	def _compute_local(self, system, step):
		"""
		Compute local parameter according to subcell subdivision.
		Akin to PropertiesBase.compute(). 
		"""
		# NOTE: we also initialize `self.box`, assuming simulation cell size is constant
		# throughout steps of a trajectory

		# Setup subcell numbers
		self.total_subcells = self.number_subcells**self.ndim

		# Create grid structure
		self.cell = system.cell
		self.box = self.cell.side
		self.side_subcell = self.box/self.number_subcells
		"""Length of subcell side"""

		# Compute or update particle list in subcells
		positions = system.view('position')
		self._sort_particles(positions)
		
		subcell_parameter = []
		for i in range(self.total_subcells):
			# TODO: find a more efficient way
			# Positionality isn't needed since we're not looking for neighbors
			variable = system.view(self.field)[self.subcell_particles[i]]
			subcell_parameter.append(self.parameter(variable))

		self.db[step] = subcell_parameter

	# Global parameter facilities
	def _compute_global(self, system, step):
		variable = system.view(self.field)
		self.db[step] = self.parameter(variable)

	def compute(self, system, step, scope='global'):
		"""Interface for local and global compure function."""
		if scope == 'global':
			self._compute_global(system, step)
		elif scope == 'local':
			self._compute_local(system, step)
		else:
			raise ValueError(f'wrong scope {scope}')
		
	@property
	def time(self):
		"""Extract frame number from callback dictionary."""
		return list(self.db.keys())

	@property
	def dt(self):
		"""Compute time interval between frames."""
		return list(np.diff(self.time, prepend=-self.time[1]))

	@property
	def tau(self):
		"""Compute timesteps."""
		return list(np.asarray(self.time)//np.asarray(self.dt))

	@property
	def time_series(self):
		"""Extract parameter time series from callback dictionary."""
		return list(self.db.values())

	@property
	def average(self):
		"""Compute parameter average."""
		return np.mean(self.time_series, axis=0)

	@property
	def variance(self):
		"""Compute parameter variance."""
		variance = (np.mean(np.asarray(self.time_series)**2)-self.average**2)
		return variance

	@property
	def histogram(self):
		"""Generate histogram from parameter time series. It also computes bin centers for easy plotting."""
		histogram, self.bin_edges = np.histogram(
			self.time_series, density=True, bins=self.histogram_bins)
		self.bin_centers = (self.bin_edges[:-1] + self.bin_edges[1:]) / 2
		return histogram
'''
