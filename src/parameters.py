import numpy as np

from src.base import PropertiesBase, LocalBase
from src.correlation import CorrelationBase

# Parameter base class


# (MergedBase, CorrelationBase):
class Parameter(PropertiesBase, CorrelationBase, LocalBase):
    """
    Interface for parameter classes
    Its purpose is to minimize interface and inheritance issues with specific parameter classes. A simple, standardized interface is provided, which hides all the *Base classes.
    """

    def __init__(self, trajectory, metadata):
        super().__init__(trajectory=trajectory, metadata=metadata)

        self.field = None
        self.name = None
        self.short_name = None

# Specific parameters


class Phi(Parameter):
    """Order parameter for Vicsek model simulations. """

    def __init__(self, trajectory, metadata):
        super().__init__(trajectory=trajectory, metadata=metadata)

        self.field = 'velocity'
        self.name = 'order parameter'
        self.short_name = 'phi(t)'

    def parameter(self, velocity):
        """Compute order parameter"""
        # Sanity check on array order
        if velocity.flags['C_CONTIGUOUS']:
            velocity = np.transpose(velocity)
        norm = np.linalg.norm(np.sum(velocity, axis=1))/self.npart
        return norm/self.v0

    @property
    def variance(self):
        """Compute parameter variance."""
        variance = (np.mean(np.asarray(self.time_series)**2)-self.average**2)
        return self.L**(int(self.ndim))*variance

    @property
    def variance_local(self):
        """Compute parameter variance."""
        variance = (np.mean(np.asarray(self.time_series_local)
                    ** 2)-self.average_local**2)
        return self.L**(int(self.ndim))*variance

    @property
    def binder(self):
        """
        Compute Binder's fourth order cumulant.
        Since it basically is a ratio of moments of PDF(phi(t)), it belongs to Phi
        """
        return 1. - np.mean(np.power(self.time_series, 4))/(3.*np.mean(np.power(self.time_series, 2))**2)

    def analyze(self, **kwargs):
        super().analyze(**kwargs)
        self.analysis['binder parameter'] = self.binder


class Theta(Parameter):
    """Orientation. """

    def __init__(self, trajectory, metadata):
        super().__init__(trajectory=trajectory, metadata=metadata)

        self.field = 'orientation'
        self.name = 'orientation'
        self.short_name = 'theta(t)'

    def parameter(self, orientation):
        """Compute average orientation. Callback for Simulation or Trajectory"""
        # Sanity check on array order
        if orientation.flags['C_CONTIGUOUS']:
            orientation = np.transpose(orientation)
        return np.mean(orientation)
