active-analysis
=============

**active-analysis** is a reproducible research project which implements a workflow for data analysis of active matter simulations.

## Scope

The project aims to provide a workflow to analyze trajectory data from active matter simulations, more specifically Vicsek model simulations generated using [active](https://framagit.org/atooms/active)).

This work is based on Python and bash scripts, to be executed by terminal. 

## Description

The template can be run in its completeness with the command `./make all` or it can be run step by step as explained below.

### Step 1: Creating a reference folder

A folder containing existing reference data and plots from previous runs can be created with `./make reference`. A copy of the data and plots obtained by the reaserch is saved and can be used for comparing results obtained with following runs.

### Step 2: Creating virtual setup

With the command `./make setup` a virtual environment is created and all the needed dependencies are installed. 

Dependencies:
- atooms (3.17.1)
- atooms.active (1.0.4)
- atooms.database (0.3.1)
- numpy (1.24.0)
- matplotlib (3.7.1)
- imageio (2.33.0)

### Step 3: Data production
The command `./make production` generates a dataset based on the provided
`metadata.log` file, which contains all of the simulation parameters needed for
the current run.

Users can provide their own `metadata.log` file by editing the one provided
with this package; similarly, finished trajectory files from [active](https://framagit.org/atooms/active) API can be loaded, simply by placing them in the `input/` folder.

### Step 4: Workflow

The command `./make workflow` reproduces the entire data analysis workflow. It is divided in several steps
- `analysis`: trajectories generated in production (or user-provided) are analyzed by computing some crucial parameters and time correlations
- `plots`: analysis results are plotted.  

Every step can be executed individually.

### Step 4: Compare data with reference run

The reference data can be used for comparison. By running `./make check`, data from the reference run is loaded and compared with data from the last workflow run. 

## Authors:

Iacopo Ricci: https://iricci.frama.io/

Thanks to Prof. Daniele Coslovich for his careful supervision and for his profound insights (and [this template](https://framagit.org/coslo/template-project)!).
