#!/bin/bash
# Setup

# The convenience =make= script allows for batch execution of full workflow and its individual parts. The targets are included using the templating no-web syntax to create different project variants.

export PYTHONWARNINGS="ignore"

. src/logger.sh

line() {
    printf '%.s-' $(seq 1 $(tput cols))
    echo
}

# Exit immediately on error
set -o errtrace
trap "echo; echo ERROR: failed to execute target $1" ERR

# Make sure python environment is activated
# It is not deactivated on exit
[ -d env ] && [ -z $(command -v deactivate) ] && . env/bin/activate

case $1 in
      all)
	  start
      ./$0 reference && \
	  ./$0 clean && \
	  ./$0 setup && \
      ./$0 production && \
	  ./$0 workflow && \
 	  echo -e "\nWorkflow reproduced!"
	  stop
	  ;;
      setup)
	  echo -e "\nEnvironment setup"; line
	  ./00_setup.sh
	  ;;
      reference)
	  echo -e "\nReference data and plots folders"; line
      [ ! -d data ] && echo "No analysis data found" && exit 0
      [ ! -d plots ] && echo "No plots found" && exit 0
	  [ -d reference ] && echo "Reference folder exists" && exit 0
	  mkdir reference/
	  rsync -a data plots input reference/
	  ;;
      workflow)
	  start
	  ./$0 analysis && \
	  ./$0 plots && \
	  stop
	  ;;
      production)
	  start
	  echo -e "\nProduction"; line
	  for f in 01_* ; do ./$f; done
	  stop
	  ;;
      analysis)
	  start
	  echo -e "\nAnalysis"; line
	  for f in 02_* ; do ./$f; done
	  stop
	  ;;
      plots)
	  start
	  echo -e "\nPlots"; line
	  for f in 03_* ; do ./$f; done
	  stop
	  ;;
      check) 
      # Check consistency between current analysis and backup
	  [ ! -d reference/ ] && echo "Cannot check because backup does not exist" && exit 1
	  error=
	  start
	  echo -e "\nCheck consistency"; line
	  for f in 04_* ; do ./$f; done
	  stop
	  ;;
      clean)
	  echo -e "\nClear the dataset from the artefacts of the analysis"; line
  	  rm -rf data/ plots/
	  ;;
      veryclean)
	  echo -e "\nCompletely clear the dataset, reference/ and python environment"; line
	  read -p "Are you sure? [y/Y] " -n 1 -r
	  [[ ! $REPLY =~ ^[Yy]$ ]] && exit 0
	  ./$0 clean
	  rm -rf env/
	  ;;
      *)
	  echo "./make [all|setup|workflow|reference|check|clean|veryclean]"
	  ;;
esac
