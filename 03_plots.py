#!/usr/bin/env python
# Plots go into `plots/`.

import logging
import os
from collections import defaultdict
from datetime import datetime

import numpy as np
from atooms.core.utils import setup_logging

from src.metadata import Metadata
from src.parameters import Phi, Theta
from src.helpers import _split_dict
from src.plot import Plot

# NOTE: perhaps split into 03a_trajectory_plots, 03b_batch_plots

# Paths
current_directory = os.getcwd()
file_inp = os.path.join(current_directory, 'input/')
file_out = os.path.join(current_directory, 'data/')
# Create the plots/ folder
plot_directory = os.path.join(current_directory, 'plots')
if not os.path.exists(plot_directory):
    os.makedirs(plot_directory)

# Set up logging
log = setup_logging(name='wkfl', level=logging.DEBUG, filename=file_out+'workflow.log')
log.info('{} : Plotting started'.format(datetime.now()))

# Check if data folder is empty or without analysis data
assert not (os.listdir(file_out) == 0 or ('.als.' in os.listdir(file_out))), "Data is missing - either trajectories or analysis data."

# Fetch metadata
meta = Metadata(file_inp, file_out, db_name='db.json', log_init=False)
metadata = meta.load_metadata(database=False)
list_metadata = _split_dict(metadata)

# Instantiate Plot
plot = Plot(plot_directory)

# Initialize batch variables dict
batch_results = defaultdict(list)

# Iterate over each UID, load trajectory
for simulation in list_metadata:
    # Load trajectory with proper UID
    trajectory_filename = simulation['uid'] + '.xyz'
    trajectory_path = os.path.join(file_inp, trajectory_filename)

    # Instantiate desired parameter classes
    phi = Phi(trajectory_path, simulation)
    theta = Theta(trajectory_path, simulation)
    phi.output_path = file_out
    theta.output_path = file_out

    # Read data from `.als.` files corresponding to specific parameters
    phi.read()
    theta.read()

    # Plot single instance-related data (global)
    # 1. Order parameter time series
    plot.plot(phi.time, phi.time_series, filename=simulation['uid']+'_phi', kind='line', xlab='Time', ylab=phi.name, ylim=[
              0.0, 1.0], number_minor_x=2, number_minor_y=2, size=[10, 5])

    # 2. Autocorrelation time series
    plot.plot(phi.time, phi.autocorrelation_norm, filename=simulation['uid']+'_acfnorm', xlab='Time', ylab='Autocorrelation function', xlim=[
              0, 800], ylim=[-0.5, 1], yerr=np.abs(phi.autocorrelation_error), skiperr=5)

    # 3. Order parameter histogram
    phi.histogram(histogram_bins=70)
    plot.plot(phi.bin_centers, phi.histogram,
              filename=simulation['uid']+'_histo', kind='line', ylab=phi.name+' frequency')

    # 4. Trajectory movie
    # plot.trajectory_inp = trajectory_path
    # plot.arrowplot_movie(skip=5, duration=100, rmfiles=True, movie_name=simulation['uid']+'_movie')

    # Collate and write to file batch data for further batch processing
    # TODO: there must be a better way to do this
    batch_results['uid'].append(simulation['uid'])
    batch_results['eta'].append(simulation['eta'])
    batch_results['phi_avg'].append(phi.average)
    batch_results['phi_variance'].append(phi.variance)
    batch_results['binder'].append(phi.binder)
    batch_results['t_corr'].append(phi.autocorrelation_time)
    batch_results['t_corr_err'].append(phi.autocorrelation_time_error)

    log.info('{} : Plots complete for {}'.format(datetime.now(), simulation['uid']))

# Batch processing
batch_hash = meta.batch_to_md5(batch_results['uid'])
log.info('{} : Dataset hash: {}'.format(datetime.now(), batch_hash))

# Get sorted indices by eta
ids = np.argsort(batch_results['eta'])

# 1. Order parameter on noise
plot.plot(np.array(batch_results['eta'])[ids], np.array(batch_results['phi_avg'])[ids], filename='batch_phi', kind='scatterline', xlab='Noise', ylab=phi.name, ylim=[0.0, 1.0], number_minor_x=2, number_minor_y=2, size=[10, 5])

# 2. Binder on noise
plot.plot(np.array(batch_results['eta'])[ids], np.array(batch_results['binder'])[ids], filename='batch_binder', kind='scatterline', xlab='Noise', ylab='Binder cumulant', ylim=[0.0, 1.0], number_minor_x=2, number_minor_y=2, size=[10, 5])

# 3. Correlation time on noise
plot.plot(np.array(batch_results['eta'])[ids], np.array(batch_results['t_corr'])[ids], filename='batch_tau', kind='scatterline', xlab='Noise', ylab='Correlation time', number_minor_x=2, number_minor_y=2, size=[10, 5])

# 4. Variance on noise
plot.plot(np.array(batch_results['eta'])[ids], np.array(batch_results['phi_variance'])[ids], filename='batch_variance', kind='scatterline', xlab='Noise', ylab=phi.name+' variance', number_minor_x=2, number_minor_y=2, size=[10, 5])

# NOTE: further analysis could include:
#       - studying specific instantaneous configurations to investigate local phi, density, ...
# 5. Search for Binder minimum (FS transition point)

# 6. Order parameter on reduced noise

# 7. Binder on reduced noise

# 8. Variance on reduced noise

log.info('{} : Plotting complete'.format(datetime.now()))
